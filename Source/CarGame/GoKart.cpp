// Fill out your copyright notice in the Description page of Project Settings.

#include "GoKart.h"

#include "Components/InputComponent.h"
#include "GoKartMovementComponent.h"
#include "MovementComponentReplication.h"
#include "Components/BoxComponent.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "UnrealNetwork.h"

// Sets default values
AGoKart::AGoKart()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bReplicateMovement = false;

	MovementComp = CreateDefaultSubobject<UGoKartMovementComponent>(FName("MovementComponent"));
	ReplicateComp = CreateDefaultSubobject<UMovementComponentReplication>(FName("ReplicationComponent"));
	MeshOffset = CreateDefaultSubobject<USceneComponent>("Mesh Offset");
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>("Mesh");
	Collider = CreateDefaultSubobject<UBoxComponent>("Collider");
	SetRootComponent(Collider);
	Mesh->SetupAttachment(MeshOffset);
	MeshOffset->SetupAttachment(Collider);
	
	
}

// Called when the game starts or when spawned
void AGoKart::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		NetUpdateFrequency =1;
	}
}



FString GetEnumText(ENetRole Role)
{
	switch (Role)
	{
	case ROLE_None:
		return "None";
	case ROLE_SimulatedProxy:
		return "SimulatedProxy";
	case ROLE_AutonomousProxy:
		return "AutonomousProxy";
	case ROLE_Authority:
		return "Authority";
	default:
		return "ERROR";
	}
}

// Called every frame
void AGoKart::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	

	DrawDebugString(GetWorld(), FVector(0, 0, 100), GetEnumText(Role), this, FColor::White, DeltaTime);
}






// Called to bind functionality to input
void AGoKart::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AGoKart::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGoKart::MoveRight);

}

void AGoKart::MoveForward(float Value)
{
	if (!MovementComp)return;
	MovementComp->SetThrottle(Value);
}


void AGoKart::MoveRight(float Value)
{
	if (!MovementComp)return;
	MovementComp->SetSteering(Value);
}

