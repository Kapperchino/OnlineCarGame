// Fill out your copyright notice in the Description page of Project Settings.

#include "MovementComponentReplication.h"
#include "GoKartMovementComponent.h"
#include "UnrealNetwork.h"


// Sets default values for this component's properties
UMovementComponentReplication::UMovementComponentReplication()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	
	SetIsReplicated(true);
}


// Called when the game starts
void UMovementComponentReplication::BeginPlay()
{
	Super::BeginPlay();
	MovementComp = GetOwner()->FindComponentByClass<UGoKartMovementComponent>();
	MeshOffset = Cast<AGoKart>(GetOwner())->GetMeshOffset();
	// ...
	
}

void UMovementComponentReplication::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UMovementComponentReplication, ServerState);
}

float UMovementComponentReplication::VelocityToDerivative()
{
	float VelocityToDerivative = ClientTimeBetweenLastUpdates * 100;
	return VelocityToDerivative;
}

void UMovementComponentReplication::OnRep_ServerState()
{
	if (GetOwnerRole() == ROLE_AutonomousProxy)
	{
		AutonomousProxy_OnRep_ServerState();
	}
	if (GetOwnerRole() == ROLE_SimulatedProxy)
	{
		SimulatedProxy_OnRep_ServerState();
	}
	

}

void UMovementComponentReplication::SimulatedProxy_OnRep_ServerState()
{
	if (!MovementComp||!MeshOffset)return;

	ClientTimeBetweenLastUpdates = ClientTimeSinceUpdate;
	ClientTimeSinceUpdate = 0;
	
	
	ClientStratTranform = MeshOffset->GetComponentTransform();

	ClientStartVelocity = MovementComp->GetVelocity();
	
	GetOwner()->SetActorTransform(ServerState.Tranform);
	
}

void UMovementComponentReplication::AutonomousProxy_OnRep_ServerState()
{
	if (!MovementComp)return;

	GetOwner()->SetActorTransform(ServerState.Tranform);
	MovementComp->SetVelocity(ServerState.Velocity);

	ClearAcknowlegedMoves(ServerState.LastMove);

	for (int x = 0; x < UnacknowlegedMoves.Num(); x++)
	{
		MovementComp->SimulateMove(UnacknowlegedMoves[x]);
	}
}

void UMovementComponentReplication::UpdateServerState(const FGoKartMove& LastMove)
{
	ServerState.LastMove = LastMove;
	ServerState.Tranform = GetOwner()->GetActorTransform();
	ServerState.Velocity = MovementComp->GetVelocity();
}

void UMovementComponentReplication::ClientTick(float DeltaTime)
{
	ClientTimeSinceUpdate += DeltaTime;
	if (ClientTimeBetweenLastUpdates < KINDA_SMALL_NUMBER)return;
	if (!MovementComp)return;
	if (!MeshOffset)return;

	FHermiteCubicSpline Spline = MakeHermitSpline();

	float LerpRatio = ClientTimeSinceUpdate / ClientTimeBetweenLastUpdates;
	
	FVector NewLocation = Spline.InterpolateLocation(LerpRatio);
	FVector NewDerivative = Spline.InterpolateDerivative(LerpRatio);
	FVector NewVelocity = NewDerivative / VelocityToDerivative();


	FQuat TargetRotation = ServerState.Tranform.GetRotation();
	FQuat StartRotation = ClientStratTranform.GetRotation();

	FQuat NewRotation = FQuat::Slerp(StartRotation, TargetRotation, LerpRatio);

	FTransform NewTransform = FTransform(NewRotation,NewLocation);

	MovementComp->SetVelocity(NewVelocity);
	MeshOffset->SetWorldTransform(NewTransform);
	

	
}

FHermiteCubicSpline UMovementComponentReplication::MakeHermitSpline()
{
	FVector TargetLocation = ServerState.Tranform.GetLocation();
	FVector StartLocation = ClientStratTranform.GetLocation();
	FVector StartDerivative = ClientStartVelocity * VelocityToDerivative();
	FVector TargetDerivative = ServerState.Velocity*VelocityToDerivative();

	FHermiteCubicSpline Spline(StartLocation, StartDerivative, TargetLocation, TargetDerivative);
	return Spline;
}

// Called every frame
void UMovementComponentReplication::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!MovementComp)return;

	AActor* Actor = GetOwner();

	FGoKartMove Move = MovementComp->GetLastMove();
	//Server and control the pawn
	if (Actor->GetRemoteRole() == ROLE_SimulatedProxy)
	{

		UpdateServerState(Move);

	}

	if (Actor->Role == ROLE_AutonomousProxy)
	{
		UnacknowlegedMoves.Add(Move);
		
		Server_SendMove(Move);
	}

	if (Actor->Role == ROLE_SimulatedProxy)
	{
		ClientTick(DeltaTime);
	}
}

void UMovementComponentReplication::ClearAcknowlegedMoves(FGoKartMove LastMove)
{
	TArray<FGoKartMove> NewMoves;

	for (int x = 0; x < UnacknowlegedMoves.Num(); x++)
	{
		if (UnacknowlegedMoves[x].Time >= LastMove.Time)
		{
			NewMoves.Add(UnacknowlegedMoves[x]);
		}
	}

	UnacknowlegedMoves = NewMoves;
}

void UMovementComponentReplication::Server_SendMove_Implementation(FGoKartMove Move)
{
	if (!MovementComp)return;
	MovementComp->SimulateMove(Move);
	UpdateServerState(Move);
	
}

bool UMovementComponentReplication::Server_SendMove_Validate(FGoKartMove Move)
{
	return true;
}

