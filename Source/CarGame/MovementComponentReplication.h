// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GoKart.h"
#include "MovementComponentReplication.generated.h"

struct FHermiteCubicSpline
{
	FVector StartLocation;
	FVector StartDerivative;
	FVector TargetLocation;
	FVector TargetDerivative;

	FVector InterpolateLocation(float LerpRatio)
	{
		return FMath::CubicInterp(StartLocation, StartDerivative, TargetLocation, TargetDerivative, LerpRatio);
	}
	FVector InterpolateDerivative(float LerpRatio)
	{
		return FMath::CubicInterpDerivative(StartLocation, StartDerivative, TargetLocation, TargetDerivative, LerpRatio);
	}
	FHermiteCubicSpline(FVector StartLocation, FVector StartDerivative, FVector TargetLocation, FVector TargetDerivative)
	{
		this->StartLocation = StartLocation;
		this->StartDerivative = StartDerivative;
		this->TargetLocation = TargetLocation;
		this->TargetDerivative = TargetDerivative;
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CARGAME_API UMovementComponentReplication : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMovementComponentReplication();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	TArray<FGoKartMove> UnacknowlegedMoves;

	class UGoKartMovementComponent* MovementComp = nullptr;

	float VelocityToDerivative();

	UFUNCTION()
	void OnRep_ServerState();

	void SimulatedProxy_OnRep_ServerState();
	void AutonomousProxy_OnRep_ServerState();

	UPROPERTY(ReplicatedUsing = OnRep_ServerState)
	FGoKartState ServerState;

	void UpdateServerState(const FGoKartMove& LastMove);

	FVector ClientStartVelocity;

	FTransform ClientStratTranform;

	USceneComponent* MeshOffset = nullptr;

	float ClientTimeSinceUpdate = 0;
	float ClientTimeBetweenLastUpdates = 0;

	void ClientTick(float DeltaTime);

	FHermiteCubicSpline MakeHermitSpline();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void ClearAcknowlegedMoves(FGoKartMove LastMove);
	
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SendMove(FGoKartMove Move);

	void Server_SendMove_Implementation(FGoKartMove Move);
	

	bool Server_SendMove_Validate(FGoKartMove Move);
	
	
	
};
