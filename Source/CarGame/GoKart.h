// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GoKart.generated.h"


USTRUCT()
struct FGoKartMove
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY()
		float Throttle;
	UPROPERTY()
		float SteeringThrow;

	UPROPERTY()
		float DeltaTime;
	UPROPERTY()
		float Time;
};


USTRUCT()
struct FGoKartState
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY()
		FTransform Tranform;

	UPROPERTY()
		FVector Velocity;

	UPROPERTY()
		FGoKartMove LastMove;
};

UCLASS()
class CARGAME_API AGoKart : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AGoKart();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	class UGoKartMovementComponent* MovementComp = nullptr;

	UPROPERTY(VisibleAnywhere)
	class UMovementComponentReplication* ReplicateComp = nullptr;

	UPROPERTY(VisibleAnywhere)
	class USceneComponent* MeshOffset = nullptr;

	UPROPERTY(VisibleAnywhere)
	class UBoxComponent* Collider = nullptr;

	UPROPERTY(VisibleAnywhere)
	class USkeletalMeshComponent* Mesh = nullptr;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	USceneComponent* GetMeshOffset()
	{
		return MeshOffset;
	}
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


private:


	

	// The mass of the car (kg).
	

	void MoveForward(float Value);
	void MoveRight(float Value);

	

	

};