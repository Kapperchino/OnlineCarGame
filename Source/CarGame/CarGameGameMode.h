// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "CarGameGameMode.generated.h"

UCLASS(minimalapi)
class ACarGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACarGameGameMode();
};



